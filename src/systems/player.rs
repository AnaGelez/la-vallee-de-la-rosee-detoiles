use amethyst::{
    core::{Transform, timing::Time},
    derive::SystemDesc,
    ecs::*,
    input::{InputHandler, StringBindings},
    assets::AssetStorage,
    audio::{output::Output, Source},
};

use crate::vallee::{Player, Mouse, Sounds};

#[derive(SystemDesc)]
pub struct PlayerSystem;

impl<'s> System<'s> for PlayerSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Mouse>,
        ReadStorage<'s, Player>,
        Read<'s, InputHandler<StringBindings>>,
        Read<'s, Time>,
        Entities<'s>,
        Read<'s, AssetStorage<Source>>,
        ReadExpect<'s, Sounds>,
        Option<Read<'s, Output>>,
    );

    fn run(&mut self, (mut transforms, mouse, players, inputs, time, entities, storage, sounds, audio_output): Self::SystemData) {
        let mut tr = None;
        for (_, trans) in (&players, &mut transforms).join() {
            let x_delta = inputs.axis_value("x").and_then(|x| if x != 0.0 { Some(x) } else { None });
            let y_delta = inputs.axis_value("y").and_then(|x| if x != 0.0 { Some(x) } else { None });
            if let Some(x_delta) = x_delta {
                trans.prepend_translation_x(x_delta * 2.0);
            }
            if let Some(y_delta) = y_delta {
                trans.prepend_translation_y(y_delta * 2.0);
            }

            if (time.frame_number() % 50) > 25 {
                trans.set_rotation_z_axis(0.2);
            } else {
                trans.set_rotation_z_axis(-0.2);
            }

            tr = Some((trans.translation().x, trans.translation().y));
        }
        if let Some((x, y)) = tr {
            for (_, mouse_tr, mouse_ent) in (&mouse, &transforms, &entities).join() {
                let mouse_rad = 50.0f32;
                let mouse_x = mouse_tr.translation().x;
                let mouse_y = mouse_tr.translation().y;
                if (mouse_x - x).abs() < mouse_rad && (mouse_y - y).abs() < mouse_rad {
                    entities.delete(mouse_ent).ok();
                    play_sound(&*sounds, time.frame_number() as usize % 4, &storage, audio_output.as_ref().map(std::ops::Deref::deref))
                }
            }
        }
    }
}

fn play_sound(sounds: &Sounds, idx: usize, storage: &AssetStorage<Source>, output: Option<&Output>) {
    if let Some(ref out) = output.as_ref() {
        if let Some(sound) = storage.get(&sounds.sounds[idx]) {
            out.play_once(sound, 1.0);
        }
    }
}
