use amethyst::core::transform::TransformBundle;
use amethyst::renderer::{
    RenderingBundle,
    types::DefaultBackend,
    plugins::{RenderFlat2D, RenderToWindow},
};
use amethyst::input::{InputBundle, StringBindings};
use amethyst::audio::AudioBundle;

mod systems;
mod vallee;
use crate::vallee::Vallee;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = amethyst::utils::application_root_dir()?;
    let game_data = amethyst::GameDataBuilder::default()
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
            .with_plugin(
                RenderToWindow::from_config(amethyst::window::DisplayConfig {
                    title: "La vallée de la rosée d'étoiles".to_owned(),
                    dimensions: Some((1080, 720)),
                    ..Default::default()
                }).with_clear([0.5, 0.9, 0.4, 1.0])
            )
            .with_plugin(RenderFlat2D::default())
        )?
        .with_bundle(
            TransformBundle::new(),
        )?
        .with_bundle(
            InputBundle::<StringBindings>::new()
                .with_bindings_from_file(app_root.join("config").join("bindings.ron"))?
        )?
        .with_bundle(AudioBundle::default())?
        .with(systems::PlayerSystem, "player_sys", &[ "input_system" ]);
    let assets_dir = app_root.join("assets");
    let mut game = amethyst::Application::new(assets_dir, Vallee, game_data)?;
    game.run();
    Ok(())
}
