use amethyst::{
    assets::{AssetStorage, Loader, Handle},
    prelude::*,
    core::transform::Transform,
    ecs::prelude::{DenseVecStorage, Component},
    renderer::{Camera, ImageFormat, SpriteSheet, SpriteRender, SpriteSheetFormat, Texture},
    audio::{OggFormat, SourceHandle},
};

const VIEW_W: f32 = 1080.0;
const VIEW_H: f32 = 720.0;

pub struct Vallee;

impl SimpleState for Vallee {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.register::<Grass>();
        let sprites = load_sprite_sheet(data.world, "grass");
        init_grass(data.world, sprites.clone(), 0.4, 0.2);
        init_grass(data.world, sprites.clone(), 0.5, 0.4);
        init_grass(data.world, sprites.clone(), 0.1, 0.6);
        init_grass(data.world, sprites.clone(), 0.7, 0.5);
        init_grass(data.world, sprites.clone(), 0.8, 0.4);
        init_grass(data.world, sprites.clone(), 0.9, 0.5);
        let sprites = load_sprite_sheet(data.world, "souris");
        init_mouse(data.world, sprites.clone(), 0.8, 0.1);
        init_mouse(data.world, sprites.clone(), 0.8, 0.5);
        init_mouse(data.world, sprites.clone(), 0.7, 0.9);
        init_mouse(data.world, sprites.clone(), 0.1, 0.2);
        let sprites = load_sprite_sheet(data.world, "chat");
        init_player(data.world, sprites);
        init_camera(data.world);
        init_audio(data.world);
    }
}

fn init_camera(world: &mut World) {
    let mut trans = Transform::default();
    trans.set_translation_xyz(VIEW_W * 0.5, VIEW_H * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(VIEW_W, VIEW_H))
        .with(trans)
        .build();
}

fn init_player(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    let mut trans = Transform::default();
    trans.set_translation_xyz(VIEW_W * 0.5, VIEW_H * 0.5, 0.0);

    let sprite = SpriteRender {
        sprite_sheet,
        sprite_number: 0,
    };
    world.create_entity()
        .with(Player)
        .with(trans)
        .with(sprite)
        .build();
}

fn init_mouse(world: &mut World, sprite_sheet: Handle<SpriteSheet>, x: f32, y: f32) {
    let mut trans = Transform::default();
    trans.set_translation_xyz(VIEW_W * x, VIEW_H * y, -1.0);

    let sprite = SpriteRender {
        sprite_sheet,
        sprite_number: 0,
    };
    world.create_entity()
        .with(Mouse)
        .with(trans)
        .with(sprite)
        .build();
}

fn init_grass(world: &mut World, sprite_sheet: Handle<SpriteSheet>, x: f32, y: f32) {
    let mut trans = Transform::default();
    trans.set_translation_xyz(VIEW_W * x, VIEW_H * y, -0.5);

    let sprite = SpriteRender {
        sprite_sheet,
        sprite_number: 0,
    };
    world.create_entity()
        .with(Grass)
        .with(trans)
        .with(sprite)
        .build();
}
fn load_sprite_sheet(world: &mut World, name: &'static str) -> Handle<SpriteSheet> {
    let text = {
        let loader = world.read_resource::<Loader>();
        let text_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            format!("textures/{}.png", name),
            ImageFormat::default(),
            (),
            &text_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        format!("textures/{}.ron", name),
        SpriteSheetFormat(text),
        (),
        &sprite_store,
    )
}

fn load_audio(loader: &Loader, world: &World, name: &'static str) -> SourceHandle {
    loader.load(name, OggFormat, (), &world.read_resource())
}

fn init_audio(world: &mut World) {
    let sounds = {
        let loader = world.read_resource::<Loader>();
        Sounds {
            sounds: [
                load_audio(&loader, &world, "sounds/yo.ogg"),
                load_audio(&loader, &world, "sounds/mercibonjour.ogg"),
                load_audio(&loader, &world, "sounds/cmaran.ogg"),
                load_audio(&loader, &world, "sounds/capulse.ogg"),
            ]
        }
    };

    world.insert(sounds);
}

pub struct Sounds {
    pub sounds: [SourceHandle; 4],
}

pub struct Player;

impl Component for Player {
    type Storage = DenseVecStorage<Self>;
}

pub struct Mouse;
impl Component for Mouse {
    type Storage = DenseVecStorage<Self>;
}

pub struct Grass;
impl Component for Grass {
    type Storage = DenseVecStorage<Self>;
}
